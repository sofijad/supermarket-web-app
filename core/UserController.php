<?php
    namespace App\Core;

    class UserController extends Controller {
        public function __pre() {
            if (!$this->getSession()->get('administratorId', null)) {
                ob_clean();
                header('Location: ' . BASE . 'admin/prijava/', true, 307);
                exit;
            }
        }
    }
