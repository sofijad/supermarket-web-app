function validateForm() {
    let status = true;

    document.querySelector('#error-message').classList.add('d-none');
    document.querySelector('#error-message').innerHTML = '';

    const title = document.querySelector('#title').value;
    if(!title.match(/.*[^\s]{3,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Naslov mora sadržati najmanje tri vidljiva karaktera. </br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    const content = document.querySelector('#content').value;
    if(!content.match(/.*[^\s]{10,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Sadržaj mora sadržati najmanje deset vidljivih karaktera. </br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    return status;
}