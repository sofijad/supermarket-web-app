<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\ProductModel;
use App\Models\AdministratorModel;
use App\Validators\StringValidator;

class LoginController extends Controller {



    public function loginGet() {
        $this->getSession()->clear();
    }

    

    public function loginPost() {
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $am = new AdministratorModel($this->getDatabaseConnection());   
        $administrator = $am->getByFieldName('username', $username);

        if (!$administrator) {
            sleep(1);
            $this->set('message', 'Uneli ste netačne podatke.');
            return;
        }
        if (!password_verify($password, $administrator->password_hash)) {
            sleep(1);
            $this->set('message', 'Uneli ste netačne podatke.');
            return;
        }        

        $this->getSession()->put('administratorId', $administrator->administrator_id);

        \ob_clean();
        header('Location: ' . BASE . 'admin/panel/');
        exit;
    }

    public function logoutGet(){
        $this->getSession()->remove('administratorId');
        $this->getSession()->save();
        header('Location: ' . BASE . 'admin/prijava');
        exit;
    }

}