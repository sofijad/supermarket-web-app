<?php
namespace App\Controllers;

use App\Core\UserController;
use App\Models\ContactModel;
use App\Models\ArticleModel;
use App\Models\ArticleCategoryModel;
use App\Models\ImageModel;
use App\Models\ProductModel;
use App\Models\ProductCategoryModel;
use App\Models\PageModel;

class AdminDashboardController extends UserController {
    public function panel() {
        $cm = new ContactModel($this->getDatabaseConnection());        
        $contacts = $cm->getAllSortedByLatest('created_at');             
        $this->set('contacts', $contacts);

        $acm = new ArticleCategoryModel($this->getDatabaseConnection());
        $articleCategories = $acm->getAll();
        $this->set('articleCategories', $articleCategories);

        $am = new ArticleModel($this->getDatabaseConnection());
        $articles = $am->getAll();
        $this->set('articles', $articles);

        $pm = new PageModel($this->getDatabaseConnection());
        $pages = $pm->getAll();
        $this->set('pages', $pages);

        $im = new ImageModel($this->getDatabaseConnection());
        $images = $im->getAll();
        $this->set('images', $images);

        $pcm = new ProductCategoryModel($this->getDatabaseConnection());
        $productCategories = $pcm->getAll();
        $this->set('productCategories', $productCategories);

        $productModel = new ProductModel($this->getDatabaseConnection());
        $products = $productModel->getAll();
        $this->set('products', $products);
    }
    

    public function contactGetEdit($id) { 
        $cm = new ContactModel($this->getDatabaseConnection());

        $contact = $cm->getById($id);

        if (!$contact) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/panel');
            exit;
        }

        $this->set('contact', $contact);
    }

    public function contactPostEdit($id) {
        $forename = filter_input(INPUT_POST, 'forename', FILTER_SANITIZE_STRING);
        $surname = filter_input(INPUT_POST, 'surname', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);            

        if (isset($_POST['submit'])) {
            $is_visible = 1;
        }
        elseif (isset($_POST['is_visible'])) {
            $is_visible = 0;
        }
        
        $cm = new ContactModel($this->getDatabaseConnection());

        $contact = $cm->editById($id, [
            'forename' => $forename,
            'surname' => $surname,
            'email' => $email,
            'message' => $message,
            'is_visible' => $is_visible
        ]);

        
        if (!$contact) {
            $this->set('message', 'Došlo je do greške prilikom izmene kontakt poruke.');
            return;
        }

        $this->set('message', 'Kontakt poruka je izmenjena.');
    }

    public function articleGetAdd() { 
        $acm = new ArticleCategoryModel($this->getDatabaseConnection());
        $articleCategories = $acm->getAll();
        $this->set('articleCategories', $articleCategories);
    }

    public function articlePostAdd() { 
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $text = filter_input(INPUT_POST, 'text', FILTER_SANITIZE_STRING);
        $article_category_id = filter_input(INPUT_POST, 'article_category_id', FILTER_SANITIZE_NUMBER_INT);

        $am = new ArticleModel($this->getDatabaseConnection());
        $article = $am->add([
            'title' => $title,
            'text' => $text,
            'image_path' => rand(1000, 9999),
            'is_visible' => 1       
        ]);



        if (!($article)) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja vesti.');
            return;
        }

        if (!$this->doUpload('image', $product)) { 
            return;
        }

        $this->set('message', 'Vest je dodata.'); 
    }

    public function articleCategoryGetAdd() {
        
    }
    
    public function articleCategoryPostAdd() {
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

        $acm = new ArticleCategoryModel($this->getDatabaseConnection());

        $articleCategoryId = $acm->add([
            'name' => $name,
            "is_visible" => 1
        ]);
        
        if (!$articleCategoryId) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja nove kategorije vesti.');
            return;
        }

        \ob_clean();
        header('Location: ' . BASE . 'admin/panel');
        exit;
    }

    public function articleCategoryGetEdit($id) {
        $acm = new ArticleCategoryModel($this->getDatabaseConnection());

        $articleCategory = $acm->getById($id);

        if (!$articleCategory) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/panel');
            exit;
        }

        $this->set('articleCategory', $articleCategory);

    }

    public function articleCategoryPostEdit($id) {    
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);           

        if (isset($_POST['submit'])) {
            $is_visible = 1;
        }
        elseif (isset($_POST['is_visible'])) {
            $is_visible = 0;
        }
        
        $acm = new ArticleCategoryModel($this->getDatabaseConnection());

        $articleCategory = $acm->editById($id, [
            'name' => $name,
            'is_visible' => $is_visible
        ]);

        
        if (!$articleCategory) {
            $this->set('message', 'Došlo je do greške prilikom izmene kategorije vesti.');
            return;
        }

        $this->set('message', 'Kategorija vesti je izmenjena.');        
    }

    private function doUpload($fieldName, $filename) {
        unlink(\Configuration::UPLOAD_DIR . $filename . '.jpeg');

        $path = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
        $file = new \Upload\File($fieldName, $path);
        $file->setName($filename);
        $file->addValidations([
            new \Upload\Validation\Mimetype('image/jpeg'),
            new \Upload\Validation\Size('3M')
        ]);

        try {
            $file->upload();
            return true;
        } catch (\Exception $e) {
            $this->set('message', 'Došlo je do greške: ' . implode(', ', $file->getErrors()));
            return false;
        }
    }

    public function pageGetAdd() { 

    }

    public function pagePostAdd() { 
        $content = filter_input(INPUT_POST, 'content', FILTER_SANITIZE_STRING);
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);        

        $pm = new PageModel($this->getDatabaseConnection());
        $pageId = $pm->add([
            'content' => $content,
            'image_path' => rand(1000, 9999),
            'title' => $title,
            'is_visible' => 1            
        ]);

        if (!$pageId) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja nove stranice.');
            return;
        }

        if (!$this->doUpload('image', $pageId)) { 
            return;
        }

        $this->set('message', 'Stranica je dodata.'); 
    }

    public function pageGetEdit($id) { 
        $pm = new PageModel($this->getDatabaseConnection());

        $page = $pm->getById($id);

        if (!$page) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/panel');
            exit;
        }

        $this->set('page', $page);
    }

    public function pagePostEdit($id) { 
        $content = filter_input(INPUT_POST, 'content', FILTER_SANITIZE_STRING);
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);           

        if (isset($_POST['submit'])) {
            $is_visible = 1;
        }
        elseif (isset($_POST['is_visible'])) {
            $is_visible = 0;
        }
        
        $pm = new PageModel($this->getDatabaseConnection());

        $page = $pm->editById($id, [
            'content' => $content,
            'image_path' => rand(1000, 9999),
            'title' => $title,
            'is_visible' => $is_visible
        ]);

        
        if (!$page) {
            $this->set('message', 'Došlo je do greške prilikom izmene stranice.');
            return;
        }

        if(isset($_FILES['image']) && $_FILES['image']['error'] == 0) {
            $uploadStatus = $this->doUpload('image', $page);
            if (!$uploadStatus) { 
                return;
            }
        }

        $this->set('message', 'Stranica je izmenjena.');   

    }

    public function imageGetAdd() { 

    }

    public function imagePostAdd() { 
        $shortDescription = filter_input(INPUT_POST, 'short_description', FILTER_SANITIZE_STRING);    

        $im = new ImageModel($this->getDatabaseConnection());
        $imageId = $im->add([
            'image_path' => rand(1000, 9999),
            'short_description' => $shortDescription,            
            'is_visible' => 1            
        ]);

        if (!$imageId) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja nove slike u galeriju.');
            return;
        }

        if (!$this->doUpload('image', $imageId)) { 
            return;
        }

        $this->set('message', 'Slika je dodata u galeriju.'); 
    }

    public function imageGetEdit($id) { 
        $im = new ImageModel($this->getDatabaseConnection());

        $image = $im->getById($id);

        if (!$image) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/panel');
            exit;
        }

        $this->set('image', $image);
    }

    public function imagePostEdit($id) { 
        $shortDescription = filter_input(INPUT_POST, 'short_description', FILTER_SANITIZE_STRING);           

        if (isset($_POST['submit'])) {
            $is_visible = 1;
        }
        elseif (isset($_POST['is_visible'])) {
            $is_visible = 0;
        }
        
        $im = new ImageModel($this->getDatabaseConnection());

        $imageId = $im->editById($id, [
            'image_path' => rand(1000, 9999),
            'short_description' => $shortDescription,
            'is_visible' => $is_visible
        ]);

        
        if (!$imageId) {
            $this->set('message', 'Došlo je do greške prilikom izmene slike iz galerije.');
            return;
        }

        if(isset($_FILES['image']) && $_FILES['image']['error'] == 0) {
            $uploadStatus = $this->doUpload('image', $imageId);
            if (!$uploadStatus) { 
                return;
            }
        }

        $this->set('message', 'Slika u galeriji je izmenjena.');   

    }

    public function productCategoryGetAdd() { 
        $pcm = new ProductCategoryModel($this->getDatabaseConnection());
        $productCategories = $pcm->getAll();
        $this->set('productCategories', $productCategories);
    }

    public function productCategoryPostAdd() { 
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING); 
        $productCategoryParentId = filter_input(INPUT_POST, 'product_category_id', FILTER_SANITIZE_NUMBER_INT);

        $pcm = new ProductCategoryModel($this->getDatabaseConnection());
        $productCategory = $pcm->add([
            'name' => $name,
            'product_category_parent_id' => $productCategoryParentId,            
            'is_visible' => 1            
        ]);

        if (!$productCategory) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja nove kategorije proizvoda.');
            return;
        }

        $this->set('message', 'Kategorija proizvoda je dodata.'); 
    }

    public function productCategoryGetEdit($id) { 
        $pcm = new ProductCategoryModel($this->getDatabaseConnection());       

        $productCategory = $pcm->getById($id);
        $productCategories = $pcm->getAll();

        if (!$productCategory) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/panel');
            exit;
        }
        
        $this->set('productCategories', $productCategories);
        $this->set('productCategory', $productCategory);
    }

    public function productCategoryPostEdit($id) { 
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING); 
        $productCategoryParentId = filter_input(INPUT_POST, 'product_category_id', FILTER_SANITIZE_NUMBER_INT);          

        if (isset($_POST['submit'])) {
            $is_visible = 1;
        }
        elseif (isset($_POST['is_visible'])) {
            $is_visible = 0;
        }
        
        $pcm = new ProductCategoryModel($this->getDatabaseConnection());  

        $productCategory = $pcm->editById($id, [
            'name' => $name,
            'product_category_parent_id' => $productCategoryParentId,
            'is_visible' => $is_visible
        ]);
        
        if (!$productCategory) {
            $this->set('message', 'Došlo je do greške prilikom izmene kategorije proizvoda.');
            return;
        }

        $this->set('message', 'Kategorija proizvoda je izmenjena.');   

    }



    public function productGetAdd() { 
        $pcm = new ProductCategoryModel($this->getDatabaseConnection());
        $productCategories = $pcm->getAll();
        $this->set('productCategories', $productCategories);
    }

    public function productPostAdd() { $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
        $product_category_id = filter_input(INPUT_POST, 'product_category_id', FILTER_SANITIZE_NUMBER_INT);
        $manufacturer = filter_input(INPUT_POST, 'manufacturer', FILTER_SANITIZE_STRING);

        $pm = new ProductModel($this->getDatabaseConnection());

        $product = $pm->add([
            'name' => $name,
            'price' => $price,
            'image_path' => rand(1000, 9999),
            'description' => $description,
            'product_category_id' => $product_category_id,
            'is_visible' => 1,
            'manufacturer' => $manufacturer            
        ]);

        if (!$product) {
            $this->set('message', 'Došlo je do greške prilikom dodavanja proizvoda.');
            return;
        }

        if (!$this->doUpload('image', $product)) { 
            return;
        }

        $this->set('message', 'Proizvod je dodat.'); 
    }

    public function productGetEdit($id) { 
        $pcm = new ProductCategoryModel($this->getDatabaseConnection());
        $productCategories = $pcm->getAll();
        $this->set('productCategories', $productCategories);        

        $pm = new ProductModel($this->getDatabaseConnection());       

        $product = $pm->getById($id);

        if (!$product) {
            \ob_clean();
            header('Location: ' . BASE . 'admin/panel');
            exit;
        }
        
        $this->set('product', $product);
    }

    public function productPostEdit($id) { 
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
        $product_category_id = filter_input(INPUT_POST, 'product_category_id', FILTER_SANITIZE_NUMBER_INT);
        $manufacturer = filter_input(INPUT_POST, 'manufacturer', FILTER_SANITIZE_STRING);      

        if (isset($_POST['submit'])) {
            $is_visible = 1;
        }
        elseif (isset($_POST['is_visible'])) {
            $is_visible = 0;
        }

        
        $pm = new ProductModel($this->getDatabaseConnection());

        $product = $pm->editById($id, [
            'name' => $name,
            'price' => $price,
            'image_path' => rand(1000, 9999),
            'description' => $description,
            'product_category_id' => $product_category_id,
            'is_visible' => $is_visible,
            'manufacturer' => $manufacturer            
        ]);
        
        if (!$product) {
            $this->set('message', 'Došlo je do greške prilikom izmene proizvoda.');
            return;
        }

        $this->set('message', 'Proizvod je izmenjen.');   

    }
    

}
