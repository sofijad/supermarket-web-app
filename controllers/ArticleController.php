<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\ArticleModel;
use App\Models\ArticleCategoryModel;

class ArticleController extends Controller {    

    public function show() {
        $am = new ArticleModel($this->getDatabaseConnection());
        $acm = new ArticleCategoryModel($this->getDatabaseConnection());
        //$aacm = new ArticleArticleCategoryModel($this->getDatabaseConnection());
        $articles = $am->getAllSortedByLatest('published_at');
        /*foreach($articles as $article){
            $category = $aacm->getById($article->article_article_category_id);
            $article->category = $category->name;
        }*/

        $articleCategories = $acm->getAll();

        $this->set('articles', $articles);
        $this->set('articleCategories', $articleCategories);
    }
    
    public function showCategoryAuctions($articleCategoryId) {
        $am = new ArticleModel($this->getDatabaseConnection());
        $articles = $am->getAllLatestArticlesByCategoryId($articleCategoryId);
        $this->set('articles', $articles);

        $acm = new ArticleCategoryModel($this->getDatabaseConnection());
        $articleCategory = $acm->getById($articleCategoryId);
        $this->set('articleCategory', $articleCategory);

        $articleCategories = $acm->getAll();
        $this->set('articleCategories', $articleCategories);
    }
}
