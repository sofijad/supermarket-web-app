<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\ContactModel;

class ContactController extends Controller {    

    public function contactGet() {
    }

    public function contactPost() {
        $forename = filter_input(INPUT_POST, 'forename', FILTER_SANITIZE_STRING);
        $surname = filter_input(INPUT_POST, 'surname', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);    
        
        $cm = new ContactModel($this->getDatabaseConnection());

        $contact = $cm->add([
            'forename' => $forename,
            'surname' => $surname,
            'email' => $email,
            'message' => $message,
            'is_visible' => 1
        ]);


        if (!$contact) {
            $this->set('message', 'Došlo je do greške prilikom slanja Vaše poruke.');
            return;
        }

        $this->set('message', 'Vaša poruka je poslata administratorima sajta.');
    }


}
