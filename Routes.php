<?php
    use App\Core\Route;

    return [
        # Rute vidljive posetiocima
        Route::get('#^admin/prijava/?$#',                            'Login',                     'loginGet'),
        Route::post('#^admin/prijava/?$#',                           'Login',                     'loginPost'),
        Route::get('#^admin/odjava/?$#',                             'Login',                     'logoutGet'),
        
        /*Route::get('#^vesti/?$#',                                    'Article',                  'show'),
        Route::get('#^vesti/([0-9]+)/?$#',                           'Article',                  'showArticlesByCategory'),      
    
        Route::get('#^kontakt/?$#',                                  'Contact',                  'contactGet'),
        Route::post('#^kontakt/?$#',                                 'Contact',                  'contactPost'),     */           
    
        #Rute vidljive administratorima          
        Route::get('#^admin/panel/?$#',                              'AdminDashboard',           'panel'),
    
        Route::get('#^kontakt/izmeni/([0-9]+)/?$#',                  'AdminDashboard',           'contactGetEdit'),
        Route::post('#^kontakt/izmeni/([0-9]+)/?$#',                 'AdminDashboard',           'contactPostEdit'),

        Route::get('#^vesti/kategorije/izmeni/([0-9]+)/?$#',         'AdminDashboard',           'articleCategoryGetEdit'),
        Route::post('#^vesti/kategorije/izmeni/([0-9]+)/?$#',        'AdminDashboard',           'articleCategoryPostEdit'),
        Route::get('#^vesti/kategorije/dodaj/?$#',                   'AdminDashboard',           'articleCategoryGetAdd'),
        Route::post('#^vesti/kategorije/dodaj/?$#',                  'AdminDashboard',           'articleCategoryPostAdd'),

        Route::get('#^proizvod/kategorije/izmeni/([0-9]+)/?$#',     'AdminDashboard',           'productCategoryGetEdit'),
        Route::post('#^proizvod/kategorije/izmeni/([0-9]+)/?$#',    'AdminDashboard',           'productCategoryPostEdit'),
        Route::get('#^proizvod/kategorije/dodaj/?$#',               'AdminDashboard',           'productCategoryGetAdd'),
        Route::post('#^proizvod/kategorije/dodaj/?$#',              'AdminDashboard',           'productCategoryPostAdd'),

        Route::get('#^proizvod/izmeni/([0-9]+)/?$#',                 'AdminDashboard',           'productGetEdit'),
        Route::post('#^proizvod/izmeni/([0-9]+)/?$#',                'AdminDashboard',           'productPostEdit'),
        Route::get('#^proizvod/dodaj/?$#',                           'AdminDashboard',           'productGetAdd'),
        Route::post('#^proizvod/dodaj/?$#',                          'AdminDashboard',           'productPostAdd'),

        Route::get('#^slika/izmeni/([0-9]+)/?$#',                    'AdminDashboard',           'imageGetEdit'),
        Route::post('#^slika/izmeni/([0-9]+)/?$#',                   'AdminDashboard',           'imagePostEdit'),
        Route::get('#^slika/dodaj/?$#',                              'AdminDashboard',           'imageGetAdd'),
        Route::post('#^slika/dodaj/?$#',                             'AdminDashboard',           'imagePostAdd'),

        Route::get('#^strana/izmeni/([0-9]+)/?$#',                   'AdminDashboard',           'pageGetEdit'),
        Route::post('#^strana/izmeni/([0-9]+)/?$#',                  'AdminDashboard',           'pagePostEdit'),
        Route::get('#^strana/dodaj/?$#',                             'AdminDashboard',           'pageGetAdd'),
        Route::post('#^strana/dodaj/?$#',                            'AdminDashboard',           'pagePostAdd'),            
        
        
     
        # Fallback           
        Route::get('#^.*$#',                                         'Main',                     'home')
    ];
