<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class AdministratorModel extends Model {
        protected function getFields() {
            return [
                'administrator_id'  => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10), false),
                'forename'          => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(255)),
                'surname'           => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(255)),
                'email'             => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(255)),
                'username'          => new Field(
                                           (new StringValidator())
                                               ->setMinLength(1)
                                               ->setMaxLength(64)),
                'password_hash'     => new Field(
                                           (new StringValidator())
                                               ->setMinLength(1)
                                               ->setMaxLength(128)),
                
                
                
                'is_active'         => new Field(new BitValidator())
            ];
        }
    }
