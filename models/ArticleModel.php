<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ArticleModel extends Model {
        protected function getFields() {
            return [
                'article_id'       => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(10), false),
                'title'            => new Field(
                                        (new StringValidator())
                                            ->setMinLength(1)
                                            ->setMaxLength(50)),
                'text'             => new Field(
                                        (new StringValidator())
                                            ->setMinLength(1)
                                            ->setMaxLength(64000)),
                'published_at'     => new Field(new DateTimeValidator(), false),
                'image_path'       => new Field(
                                        (new StringValidator())
                                             ->setMinLength(1)
                                             ->setMaxLength(255)),                               
                'is_visible'       => new Field(new BitValidator())
            ];
        }

        public function getAllLatestArticlesByCategoryId(int $categoryId) :array {

            $sql = 'SELECT DISTINCT article.article_id 
                    FROM article
                    JOIN article_article_category ON article.article_id = article_article_category.article_id
                    JOIN article_category ON article_category.article_category_id = article_article_category.article_category_id
                    WHERE article_category.category_id = ' . $categoryId .
                    ' ORDER BY article.published_at DESC;';
            
            $prep = $this->getDatabaseConnection()->getConnection()->prepare($sql);
            if(!$prep) {
                return [];
            }
            $res = $prep->execute([]);
            if(!$res) {
                return [];

            }
            return $prep->fetchAll(\PDO::FETCH_OBJ);

        }               
        
        
    }