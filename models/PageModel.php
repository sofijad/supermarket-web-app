<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class PageModel extends Model {
        protected function getFields() {
            return [
                'page_id'    => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10), false),
                'content'    => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(64000)),
                'image_path' => new Field(
                                    (new StringValidator())
                                         ->setMinLength(1)
                                         ->setMaxLength(255)), 
                'title'      => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(50)),                               
                'is_visible' => new Field(new BitValidator())
            ];
        }

        
    }