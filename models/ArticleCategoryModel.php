<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ArticleCategoryModel extends Model {
        protected function getFields() {
            return [
                'article_category_id' => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10), false),
                'name'                => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(50)),                
                'is_visible'          => new Field(new BitValidator())
            ];
        }
    }

    