<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class ProductModel extends Model {
        protected function getFields() {
            return [
                'product_id'          => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10), false),
                'name'                => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(255)),
                'price'               => new Field((new NumberValidator())
                                                ->setReal()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10)
                                                ->setMaxDecimalDigits(2)),
                'image_path'          => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(255)),
                'description'         => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(64000)),                
                
                
                'product_category_id' => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10)),
                'is_visible'          => new Field(new BitValidator()),
                'manufacturer'        => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(255))
                
            ];
        }

        public function getAllVisilbleSearch($keyword) {
            $sql = 'SELECT
                        *
                    FROM
                        product
                    WHERE
                        name LIKE ? AND
                        is_visible = 1;';
            
            $prep = $this->getDatabaseConnection()->getConnection()->prepare($sql);

            $res = $prep->execute([ '%' . $keyword . '%' ]);

            if ($res) {
                return $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return [];
        }
    }